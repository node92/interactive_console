require('colors');

const { saveInfo, readInfo } = require('./helpers/saveFile');
const {
    inquirerMenu,
    pause,
    readInput,
    listTasksDelete,
    confirm,
    showListChecklist
} = require('./helpers/inquirer');

const Tasks = require('./models/tasks');

console.clear();

const main = async () => {

    let opt = ' ';
    const tasks = new Tasks();

    const tasksInfo = readInfo();

    if (tasksInfo) {
        tasks.uploadTasksFromArray(tasksInfo);
    }

    do {
        opt = await inquirerMenu();

        switch (opt) {
            case '1':
                const desc = await readInput('Descripcion: ');
                tasks.createTask(desc);
                break;
            case '2':
                tasks.listTasksComplete();
                break;
            case '3': // list tasks complete
                tasks.listTasksPendingOrComplete(true);
                break;
            case '4': // list tasks pending
                tasks.listTasksPendingOrComplete(false);
                break;
            case '5': //  complete - pending tasks
                const ids = await showListChecklist(tasks.listArr);
                tasks.toggleCompletes( ids );
                break;
            case '6': // delete  task
                const id = await listTasksDelete(tasks.listArr);
                console.log(id);
                if (id !== '0') {
                    const confirmDelete = await confirm('¿Esta Seguro ?');
                    if (confirmDelete) {
                        tasks.deleteTask(id);
                        console.log('Tarea borrada!');
                    }
                }
                break;
        }

        saveInfo(tasks.listArr);

        await pause();

    } while (opt !== '0');
}

main();