const Task = require('./task');
require('colors');
class Tasks {

    _list = {};

    get listArr() {
        
        const list = [];
        Object.keys(this._list).forEach( key => {
            const task = this._list[key];
            list.push(task);
        });

        return list;
    }

    constructor() {
        this._list = {};
    }

    deleteTask( id = ' ' ){

        if( this._list[id] ) {
            delete this._list[id];
        }
    }
    uploadTasksFromArray( tasks = [] ){

        tasks.forEach( task => {
            this._list[task.id] = task;
        });
      
    }
    createTask(desc = ' ') {
        
        const task = new Task(desc);
        this._list[task.id] = task;

    }
    listTasksComplete(){

        console.log();
        this.listArr.forEach( ( task,index  ) => {
            const idx = `${ index + 1 }.`.green;
            const { desc,completeIn } = task;
            const status = ( completeIn ) 
                                                ? 'Complete'.green
                                                : 'Pending'.red;
            console.log( `${ idx } ${ desc } :::: ${ status}`)
        });
    }
    listTasksPendingOrComplete( complete = true ){

        console.log();
        let index = 1;
        this.listArr.forEach( task => {
      
            const { desc,completeIn } = task;

            if ( complete ){
                
                if( completeIn ){
                    index ++;
                    console.log( `${ ( index + '.').green } ${ desc } :::: ${ completeIn}`)
                }
             
            } else {

                if( !completeIn ){
                    index ++;
                    console.log( `${ ( index + '.').green } ${ desc } :::: ${ completeIn}`)
                }
            }
                                                
        });
    }
    toggleCompletes( ids = [] ) {

        ids.forEach(  id => {

            const task = this._list[id];
            if( !task.completeIn ){
                task.completeIn = new Date().toISOString();
            }
        });

        this.listArr.forEach( task => {

            if( !ids.includes(task.id) ) {
                 this._list[task.id].completeIn = null;
            }
        });
      
    }

}

module.exports = Tasks;